package com.greenblat;

import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) {

        // 1
        Random random = new Random();
        int[] arr = new int[8];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = random.nextInt(11);
        }

        // 2
        System.out.println(Arrays.toString(arr));

        // 3
        boolean isDescendingSequence = false;
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] >= arr[i + 1]) {
                System.out.println("Массив не является строго возрастающей последовательностью");
                isDescendingSequence = true;
                break;
            }
        }
        if (!isDescendingSequence)
            System.out.println("Массив является строго возрастающей последовательностью");

        // 4
        // 1 version
        for (int i = 1; i <  arr.length; i += 2) {
            arr[i] = 0;
        }
        // 2 version
        for (int i = 0; i < arr.length; i++) {
            if (i % 2 != 0) {
                arr[i] = 0;
            }
        }

        // 5
        System.out.println(Arrays.toString(arr));
    }

}